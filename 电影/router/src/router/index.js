import Vue from 'vue'
import VueRouter from 'vue-router'
import Mine from '../pages/home/mine/mine.vue'
import Cinema from '../pages/home/cinema/cinema.vue'
import Details from '../pages/datails/details.vue'
import Movie from '../pages/home/movie/movie.vue'
import NOTFOUND from '../pages/NOTFOUND.vue'
import Coming from '../pages/home/movie/coming/coming.vue'
import Hot from '../pages/home/movie/hot/hot.vue'
import Home from '../pages/home/home.vue'
import Login from '../pages/login/login.vue'
import City from '../pages/city/city.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  // 配置路由
  routes: [
    {
      path: '/',
      redirect: '/movie'
    },
    {
      path: '/home',
      component: Home,
      children: [
        {
          path: '/movie',
          component: Movie,
          redirect: '/movie/hot',
          children: [
            {
              path: '/movie/coming',
              component: Coming
            },
            {
              path: '/movie/hot',
              component: Hot
            }
          ]
        },
        {
          path: '/mine',
          component: Mine
        },
        {
          path: '/cinema',
          component: Cinema
        },
        {
          path: '/404',
          component: NOTFOUND
        }
      ]
    },
    {
      path: '/details/:id',
      name: 'details',
      component: Details,
      meta: {
        registerReg: true
      }
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/city',
      component: City
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})

// 全局前置守卫(所有页面跳转之前执行)
router.beforeEach((to, from, next) => {
  if (to.matched.some(v => v.meta.registerReg)) {
    const token = localStorage.getItem('token')
    if (token) {
      next()
    } else {
      next({
        path: '/login',
        query: {
          redirectUrl: to.fullPath
        }
      })
    }
  } else {
    next()
  }
})

// 处理重定向报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) { return originalPush.call(this, location, onResolve, onReject) }
  return originalPush.call(this, location).catch((err) => err)
}

export default router
