import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: { // 存数据的地方  获取多个mapState 组件内this.$store.state.XX 查看
    cityList: [],
    num: 20,
    curCity: [],
    value: ''
  },
  mutations: { // 修改数据的唯一方式，必须是同步函数  多个mapMutations  XXX.commit（）调用函数
    // this.$store.commit('newAddress', content)
    // state: 仓库数据
    // payload: 参数
    // 给对象添加新属性，必须使用Vue.set(对象，属性，参数)
    cityFn (state, payload) {
      state.cityList = payload
    },
    addNum (state, payload) {
      state.num += payload
    }
  },
  actions: { // 处理异步 获取数据 多个mapActions  组件内使用this.$store.dispatch('getList',参数) => 返回的是一个promise对象
    // fn (context, payload) {}
    // context类似于组件中的this.$store
    async getList (context, payload) {
      await axios.get('https://m.maizuo.com/gateway?k=7372151', {
        headers: {
          'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"16879999798557610668130305","bc":"110100"}',
          'X-Host': 'mall.film-ticket.city.list'
        }
      }).then(res => {
        context.commit('cityFn', res.data.data.cities)
      })
    }
    // ,
    // async aaa (context, payload) {
    //   context.commit('num', 2)
    // }
  },
  getters: { // 功能类似组件中的计算属性 获取多个 mapGetters
    curCity (state, payload) {
      let arr = state.cityList.filter(item => item.pinyin.includes(state.value) || item.name.includes(state.value))
      if (!state.value) arr = []
      return arr
    }
  }
})

export default store
