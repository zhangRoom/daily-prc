const copy = {
  bind (el, binding) {
    el.newcon = binding.value
    el.copyFn = () => {
      const textarea = document.createElement('textarea')
      textarea.value = el.newcon
      document.body.appendChild(textarea)
      textarea.select()
      document.execCommand('copy')
      textarea.remove()
      alert('复制成功' + el.newcon)
    }
    el.addEventListener('click', el.copyFn)
  },
  update (el, { value, oldValue }) {
    if (value !== oldValue) {
      el.newcon = value
      console.log('数据更新了', value, oldValue)
    }
  },
  unbind (el, binding) {
    el.removeEventListener('click', el.copyFn)
  }
}

export default copy
