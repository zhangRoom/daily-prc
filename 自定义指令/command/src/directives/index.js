import copy from './copy'
import loading from './loading'

const directives = {
  copy,
  loading
}

export default {
  install (Vue) {
    Object.keys(directives).forEach(key => {
      Vue.directives(key, directives[key])
    })
  }
}
